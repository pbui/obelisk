% LaTeX template for MICS papers
% To Run:  pdflatex Sample.tex

\documentclass[12pt]{article}

\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\topmargin}{0in}
\setlength{\headheight}{0in}
\setlength{\headsep}{0in}
\setlength{\textwidth}{6in}
\setlength{\textheight}{9in}
\setlength{\parindent}{0in}

\usepackage{graphicx} %For jpg figure inclusion
\usepackage{times} %For typeface
\usepackage{caption}
\usepackage{listings}
\usepackage{url}

\begin{document}
\pagestyle{plain}

\title{Obelisk: Summoning Minions on a HPC Cluster}

%\author{
%Grant Wuerker, Peter Bui\\
%Department of Computer Science\\
%University of Wisconsin - Eau Claire\\
%Eau Claire, WI 54702\\
%\{wuerkegr, buipj\}@uwec.edu
%}
\date{}

\maketitle
\thispagestyle{empty}

\section*{\centering Abstract}

In scientific research, having the ability to perform rigorous calculations in
a bearable amount of time is an invaluable asset. Fortunately, the growing
popularity of distributed systems at universities makes this a widely
accessible resource. However, in order to use such computing resources, one
must understand Linux, parallel computing, and distributed systems.
Unfortunately, most people do not have the time or patience to learn these
skills, indicating that the high performance computing (HPC) cluster has not
been abstracted far enough. The purpose of Obelisk is to facilitate the
submission of jobs via an intuitive web interface, making HPC clusters
accessible to all.  With the Obelisk web portal, users select a scientific
application, configure parameters, and submit their task to the Obelisk
manager.  This will in turn translate the request to a batch job on the HPC
cluster and provide the user with progress information via a status page.
Using Obelisk, novice users no longer need to know the arcane incantations
necessary to harness the power of HPC minions and instead can utilize a
straightforward wizard interface.

\newpage
\setcounter{page}{1}

\section{Introduction}

To someone that is familiar with Linux, parallel computing, and distributed
systems, making a high performance computing (HPC) cluster do your bidding is
relatively straightforward: you simply need to know the right commands!
However, most people do not have this type of specialized knowledge, nor should
they be expected to learn the seemingly magical incantations necessary to
harness the power of multiple machines.  For this reason, it is almost
necessary that a bridge be built between users and the cluster if the awesome
power of HPC systems is to be unleashed.  The purpose of our project, {\bf
Obelisk}, is to build this bridge and to connect users and HPC systems through
an intuitive web interface that automates the creation, submission, and
retrieval of scientific computing tasks.

\section{Design}

An overview of Obelisk is shown in Figure \ref{fig:obelisk_overview}.  Rather
than dealing with arcane Unix command shells and fighting daemons, Obelisk
allows users to simply {\tt transcribe} their computational goals and {\tt
summon} minions to perform their bidding via a responsive and easy-to-use web
application.

\begin{figure}[h]
\centerline{
    \includegraphics[height=3.0in]{obelisk-design.png}
}
\caption{Obelisk Overview}
\label{fig:obelisk_overview}
\end{figure}

\subsection{Wizard}

Often times, when a person wants to perform a task (ie. run a program) on an
HPC Cluster, they know what they want to do, but don't know how to do it.  This
is where {\bf Wizards} come into play.  When a user opens the Obelisk web
application, they will confront a list of predefined applications available on
the HPC system.  Upon selection of one of these applications, the user will be
redirected to a web page containing a dynamically generated form containing
various input fields, such as numbers, strings, files, and check-boxes.  Each
of these fields represents parameters for the command the user wishes to
execute, and includes descriptions of their purpose.

\begin{figure}[h]
\centerline{
    \includegraphics[height=3.5in]{page-wizard.png}
}
\caption{Wizard Page}
\label{fig:wizard_page}
\end{figure}

An example of a simple Wizard is shown in Figure \ref{fig:wizard_page}.  As can
be seen, each Wizard consists of a set of {\tt Elements} specific to the task
being specified along with a set of {\tt Resources} which define the
computational resources required from the HPC system.  After filling out the
form, the user may complete the Wizard and continue onto the next step by
clicking the {\tt Transcribe} button.

\subsection{Scroll}

When the user clicks on {\tt Transcribe}, the values in the form are set to the
Obelisk web application, where the data is parsed and analyzed to generate a
script that can be understood by the HPC Cluster's batch system.  In Obelisk,
this materialized script is referred to as a {\tt Scroll} and contains all the
necessary magical incantations for executing the user's specified job.  After
this script is generated, the application redirects the user's web browser to
the corresponding {\tt Scroll} page as shown in Figure \ref{fig:scroll_page}.
From here, the user can verify the results of the transcription process and
ensure Obelisk has interpreted their intentions correctly.  When the user has
approved of the script, they may click {\tt Summon} to bring the {\tt Scroll}
to life in the form of a {\tt Minion}.

\begin{figure}[h]
\centerline{
    \includegraphics[height=3.0in]{page-scroll.png}
}
\caption{Scroll Page}
\label{fig:scroll_page}
\end{figure}

\subsection{Minion}

After summoning a {\tt Scroll}, the user will be redirected to a {\tt Minion}
page. A Minion is essentially a personification of a HPC cluster batch job.
That is, a {\tt Minion} corresponds to a running task in the HPC job scheduler.
Each {\tt Minion} has a workspace, status, and output.  The user can easily
check on a job status by consulting with their {\tt Minion}. When the {\tt
Minion} has completed the {\tt Scroll}, its status will be changed to complete,
and the output files will be available for download or viewing as shown in
Figure \ref{fig:minion_page}.

\begin{figure}[h]
\centerline{
    \includegraphics[height=2.0in]{page-minion.png}
}
\caption{Minion Page}
\label{fig:minion_page}
\end{figure}

In summary, in order to mask the complexity of traditional HPC systems, Obelisk
provides an intuitive web interface that allows users to employ {\tt Wizards}
to transcribe {\tt Scrolls}, which in turn are used to summon {\tt Minions} to
perform their desired computational tasks.

\section{Implementation}

To implement Obelisk, we used a number of different web development and HPC
technologies.  The Obelisk web application itself is written in Python and the
Tornado web framework \cite{tornado}.  It is structured as a RESTful
\cite{rest} web service that receives HTTP requests (i.e. {\tt GET} and {\tt
POST} methods) and processes them via {\it handlers} to perform the appropriate
actions.  For the front-end user interface, Obelisk utilizes Bootstrap and
JQuery to mediate the interactions between users and the RESTful backend.  All
of the this activity is tracked and recorded in a database using the SQLite
library, making information pertaining to jobs readily available to users and
the program itself.  Finally, batch jobs are submitted and managed by Torque,
an open source project that is often used in HPC clusters for resource
management and allocation.  The following is a detailed explanation of how all
these components are integrated within the Obelisk to achieve the actions
described previously in the Design section.

\subsection{WizardHandler}

The user begins the process of submitting a job with Obelisk by making a HTTP
{\tt GET} request to the {\tt /wizard/} endpoint.  Internally, this is
processed by the {\tt WizardHandler}, which in this case produces a list of
{\tt Wizards} that the user may run.  These {\tt Wizards} are generated
dynamically from information inside of a collection of {\tt YAML} files stored
remotely on the web server.  When the user selects a {\tt Wizard}, they are
redirected to {\tt /wizard/wizard-name}.  

\begin{figure}[h]
\centerline{
    \includegraphics[height=3.0in]{handler-wizard.png}
}
\caption{Wizard Handler}
\label{fig:wizard_handler}
\end{figure}

Once again, this page is handled by the {\tt WizardHandler}, which parses the
corresponding {\tt YAML} file and produces an {\tt HTML} form with inputs that
determine parameters like resources allocated, queue name, error and output
file names, and parameters for the command to be run.  When the user clicks
{\tt Transcribe}, the form is sent back to {\tt /wizard/} via a {\tt POST} and
its values are converted into a batch script, also called a {\tt Scroll}, that
can be understood by Torque.  This {\tt Scroll} is stored in a unique directory
dedicated to that {\tt Scroll} that contains the script and any files uploaded
by the user.  Likewise, an entry for the {\tt Scroll} is recorded in the
database which tracks the {\tt Scrolls} name, path, creation time and
modification time.  Once all of this information is stored, the user is then
redirected to the {\tt Scroll}'s page and its contents are displayed.  This
whole process is outlined in Figure \ref{fig:wizard_handler}.

\subsection{ScrollHandler}

As with the {\tt Wizards}, a list of {\tt Scrolls} can be viewed at {\tt
/scroll/}, which is processed by the {\tt ScrollHandler}.  When the user
selects a {\tt Scroll} from the listings, they are redirected to {\tt
/scroll/scroll-id}, where they can view the content of the {\tt Scroll} and
choose to execute the script by clicking {\tt Summon} as shown in Figure
\ref{fig:scroll_handler}.  When this button is clicked, a {\tt POST} request is
made to the current page, and the job is submitted to a queue with the Torque
command, {\tt qsub}.

\begin{figure}[h]
\centerline{
    \includegraphics[height=3.0in]{handler-scroll.png}
}
\caption{Scroll Handler}
\label{fig:scroll_handler}
\end{figure}

Before submitting the job to Torque, however, the {\tt ScrollHandler} will
first create a unique {\tt Minion} directory and copy the contents of the {\tt
Scroll} directory to this new {\tt Minion} workspace.  Once this is
accomplished, Torque's {\tt qsub} command is invoked with the {\tt Scroll}
generated previously and the current working directory is set to the {\tt
Minion}'s directory.  This approach to materializing new directories for the
{\tt Scroll} and the {\tt Minion} allows users to summon multiple {\tt Minions}
from the same {\tt Scroll} without worrying about overwriting previous results.
Moreover, having separate {\tt Minion} workspaces provides sandboxing and
isolation for concurrently running batch jobs.

\subsection{MinionHandler}

Like the other handlers, the {\tt MinionHandler} provides a list of {\tt
Minions} that can be viewed by going to {\tt /minion/}.  The status and output
of any {\tt Minion} can be viewed by going to {\tt /minion/minion-id} as shown
in Figure \ref{fig:minion_handler}.  The progress of each {\tt Minion} is
updated by a timer that periodically executes {\tt qstat} on the user's jobs,
and the output files are contained inside of the {\tt Minion} workspace.  When
a file is selected, the user is redirected to an additional {\tt
MinionFileHandler} that serves the file based on its {\tt MIME} type.

\begin{figure}[h]
\centerline{
    \includegraphics[height=3.0in]{handler-minion.png}
}
\caption{Minion Handler}
\label{fig:minion_handler}
\end{figure}

\subsection{Database}

As mentioned above, Obelisk keeps track of {\tt Scrolls} and {\tt Minions} with
database tables managed by SQLite. The {\tt Scrolls} table includes the name,
id, path, creation time, and last modification time of every {\tt Scroll}. A
new row is added every time the user creates a scroll and the number of times
it is used is determined by the number of {\tt Minions} with said {\tt Scroll}
as a foreign key.  Likewise, a new {\tt Minion} is added every time the user
summons a {\tt Scroll}. The {\tt Minions} status and finish time are updated by
the timer described in the Minion handler section.

\bigskip

This timer process is shown in Figure \ref{fig:database_updater}.  Once every 5
seconds, the updater is activated and calls Torque's {\tt qstat} command with
the user's {\tt UID} to only get a listing of the user's jobs.  This
information is then parsed for the Torque job id, job status, and completion
time.  If these fields are found, then the job id is used by the updater to
lookup the corresponding {\tt Minion} and update the appropriate job status and
completion time.  Because of this approach, Obelisk can accurately report the
status of each {\tt Minion}, even in the cases of application failure.

\begin{figure}[h]
\centerline{
    \includegraphics[height=3.0in]{util-updater.png}
}
\caption{Database Updater}
\label{fig:database_updater}
\end{figure}

\subsection{DashboardHandler}

To tie all of this information together is the {\tt DashboardHandler}, which
provides an overview of the {\tt Wizards}, {\tt Scrolls}, and {\tt Minions}
available on the user's instance of Obelisk.  From this landing page, the user
can jump immediately to a {\tt Wizard} page to {\tt Transcribe} a new {\tt
Scroll}, a {\tt Scroll page} to {\tt Summon} a new {\tt Minion}, or a {\tt
Minion} page to view the progress of their work.

\subsection{Deployment}

Currently, to deploy and start Obelisk, a user needs to checkout the source
code from our Bitbucket repository: \url{https://bitbucket.org/pbui/obelisk}.
Once they have the source code and have installed the necessary dependencies
(ie. Python and Tornado), they can start it by executing the command {\tt
python obelisk} on the HPC submit node.  When this command is invoked, the web
application will be started and the IP address and port number will be
displayed to the user.  At the moment, there are no security or authentication
measures implemented, although that is on our list of future improvements.

\bigskip

The benefit of this approach is that it was simple and straightforward, while
allow us to avoid the complications of user authentication and system security.
Since the Obelisk application is executed by the owner, any jobs submitted to
the HPC batch system are also tied to the user.  Unfortunately, the current
method of starting Obelisk breaks the facade of a magical web portal for
summoning minions, so we hope to create a system for managing user instances of
Obelisk in the future.  This new system will allow users to avoid manually
deploying and invoking Obelisk; instead they would use the a portal setup by
the system administrator for accessing their Obelisk session.

\section{Evaluation}

We have tested the functionality of Obelisk and verified that it in fact works
correctly.  We will continue to add more features and fix bugs, but for now we
will focus on getting more {\tt Wizards} available for demonstration and use.
In the future, we will work with domain scientists and have science faculty and
students utilize our project to summon minions for class assignments and
research experiments.  In doing so, we will collect data that allows us to
determine the effectiveness of Obelisk and find ways to improve the user
experience.

\bigskip

To test Obelisk, we took applications that are typically ran by faculty and
students on our local HPC cluster, and converted them into Obelisk {\tt
Wizards}. By doing so, we were able to verify that Obelisk is applicable to
their computational needs and works properly.  One of these programs is
Gaussian, a computational chemistry software suite for electronic structure
modeling \cite{gaussian} that is used both for research and for teaching at our
university. The only step required in creating a {\tt Wizard} for Gaussian is
to create a {\tt YAML} file outlining its parameters and dependencies. 

\begin{figure}[h]
\lstset{numberstyle=\tiny, basicstyle=\tt\footnotesize,
    caption={Gaussian YAML},
    label={lst:gaussian_yaml},
    commentstyle=\bf,   
    captionpos=b, frame=tb, float=h, showstringspaces=false,
}
\begin{lstlisting}
icon:   'flask'
elements:
    -   name:         Name
        type:         string
        value:        g09
        description:  Name of scroll

    -   name:         Command
        type:         string
        value:        g09
        description:  Command to execute
    
    -   name:         File
        type:         file
        value:      
        description:  Com File

formula: '{Command} < {File} > Log'
\end{lstlisting}
\end{figure}

The {\tt gaussian.yaml} can be seen in Listing \ref{lst:gaussian_yaml}.  As can
be seen, the Gaussian {\tt Wizard} allows for three {\tt Elements} to be
defined by the user: {\tt Name}, {\tt Command}, and {\tt File}.  The first two
are string types which correspond to the name of the {\tt Scroll} to transcribe
and the command to execute when a {\tt Minion} is summoned.  In the {\tt HTML}
form that is generated by the {\tt WizardHandler} for this application, these
fields will take the form of standard input text fields.  The last {\tt
Element} is the input {\tt COM} file required by the Gaussian application.
Because it is marked with the {\tt file} type, the {\tt WizardHandler} will
present the user will a button they can use to select a local file to upload to
Obelisk.  The last portion of the {\tt YAML} file is the {\tt formula}, which
is the recipe used by the {\tt WizardHandler} in Obelisk to transcribe the {\tt
Elements} into a {\tt Scroll}.

\bigskip

After saving the {\tt gaussian.yaml} file, we simply need to restart Obelisk to
load the new {\tt Wizard} template.  Afterwards, we can open the {\tt Wizard}
web page and fill in the fields.  For this {\tt Wizard}, we only need to upload
a {COM} file for input into Gaussian.  There are of course other parameters for
the Gaussian program, it is by no means a limited piece of engineering.
However, for the sake of simplicity, we will use a limited set of options.
After completing the form, we can summon a {\tt Minion} to work on the {\tt
Scroll}.  The {\tt Minion} will eventually finish the {\tt Scroll}, leaving
files containing the results of the job in its workspace.  When this has
occured we can sift through the results and discover the next breakthrough in
chemistry.

\begin{figure}[h]
\centerline{
    \includegraphics[height=3.0in]{gaussian-minion.png}
    \includegraphics[height=3.0in]{gaussian-log.png}
}
\caption{Gaussian Minion and Output Log}
\label{fig:gaussian_minion}
\end{figure}

Some of this output can be seen in Figure \ref{fig:gaussian_minion}.  On the
left, we have the {\tt Minion} web page which lists the {\tt Minion} id, Torque
job id, project workspace, the executed {\tt Scroll}, the status of the {\tt
Minion}, and the start and stop times for the job.  Below this, we have a
listing of the files associated with the {\tt Minion}, including the resulting
{\tt Log} file of which a portion is shown on the right.  Altogether this
example serves to show how simple and straightforward it is for users to create
{\tt Wizards}, which in turn can be used to transcribe {\tt Scrolls}, which are
later executed by summoned {\tt Minions}, who carry out the commands of the
user on high performance computing systems.

\bigskip

As we have demonstrated, using Obelisk is relatively straightforward and easy.
There are very few barriers inhibiting researchers and students from obtaining
the results they need.  By giving the user complete control over their {\tt
Wizards}, Obelisk provides versatility, while the web based interface of
Obelisk removes the painstaking tedium involved with manually operating {\tt
Torque} from the Unix command line.  Obelisk abstracts this complexity from the
user via an intuitive and easy-to-user web application and empowers them to
harness armies of {\tt Minions} to do their bidding. 

\section{Related Work}

Our project is similar to two projects from MICS 2014: ``A Web Portal For An
Animation Render Farm" \cite{dsabr-mics2014} and ``On Ramp to Parallel
Computing" \cite{onramp-mics2014}.  Obelisk is different from the former
project in that it does not target a specific application, but rather is a
general purpose framework for different types of scientific software.  Unlike
the latter project, Obelisk provides wizards for specific scientific
applications rather than giving the user a web-based programming environment.
In some sense, Obelisk is a middle ground between the two projects, providing
an flexible and extensible but easy-to-use abstraction for HPC systems.

\bigskip

As evident by the previous work mentioned, this overall approach to
constructing web interfaces to scientific applications or high performance
computing systems is an ongoing trend in the scientific computing community.
Ian Foster notes that scientific tools tend to be difficult to deploy,
configure, and utilize effectively and that an alternative to forcing users to
become distributed system experts is to provide different service abstractions
and in particular web service applications \cite{service-oriented-science}.
For instance, XSEDE has a large listing of such "Science Gateways", which
provide simplified online access to a variety of HPC and scientific resources
around the United States \cite{xsede-gateways}.  Our project follows along in
this trend by providing a simple but effective general purpose web service for
summoning minions on HPC systems.

\section{Conclusion}

It is our belief that Obelisk helps remove some of the technical barriers
associated with scientific computing on HPC clusters.  To abstract the
complexity of harnessing these vast amounts of computing resources into a
simple but effective interface, we developed Obelisk as an intuitive web
interface to traditional HPC systems.  As demonstrated in our paper, the design
of Obelisk is relatively straightforward, while the implementation provides all
the functionality required to describe, submit, execute, and monitor scientific
computational tasks on a high performance computing cluster.  By utilizing
Obelisk, countless hours can be saved in the classroom by reducing the time
spent teaching groups of students how to use Linux and its utilities.  In the
future, we hope to create a library of programs that can easily be installed on
Obelisk and get wider user testing to fine-tune the user experience.

\begin{thebibliography}{9}

\bibitem{dsabr-mics2014}
  John Rankin, Travis Boettcher, and Peter Bui. ``A Web Portal For An Animation Render Farm". Midwest Instruction and Computing Symposium (MICS2014). Verona, WI. April, 2014.

\bibitem{onramp-mics2014}
  Zackory Erickson and Samantha Foley. ``On Ramp to Parallel Computing". Midwest Instruction and Computing Symposium (MICS2014). Verona, WI. April, 2014. 

\bibitem{gaussian}
	Gaussian.
	"Official Gaussian Website".
	http://www.gaussian.com/.
	2015.

\bibitem{tornado}
	Tornado.
	"Tornado Web Server".
	http://www.tornadoweb.org/en/stable/.
	2015.

\bibitem{rest}
	C. Pautasso, O. Zimmermann, and F. Leymann.
	"Restful web services vs. "big" web services: Making the right architectural decision". 
	In Proceedings of the 17th International Conference on World Wide Web, WWW ’08, pages 805–814, New York, NY, USA, 2008. ACM.

\bibitem{service-oriented-science}
    	I. Foster. Service-oriented science. Science, 308(5723):814–817, May 2005.

\bibitem{xsede-gateways}
	Extreme Science and Engineering Discovery Environment.
	"XSEDE Science Gateways".
	https://www.xsede.org/gateways-overview.
	2015.

\end{thebibliography}

\end{document}
