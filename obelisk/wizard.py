''' Obelisk - Wizard '''

import os
import yaml

from config import WIZARDS_PATH, SCROLLS_PATH

# System

SYSTEM_PATH = os.path.join(WIZARDS_PATH, 'torque.yaml')

# Wizard Class

class Wizard(object):
    def __init__(self, path):
        self.path = path if os.path.exists(path) else os.path.join(WIZARDS_PATH, '{}.yaml'.format(path))
        self.name = os.path.splitext(os.path.basename(self.path))[0]

        program        = yaml.load(open(self.path))
        self.icon      = program.get('icon', 'cog')
        self.elements  = program.get('elements', [])
        self.formula   = program.get('formula', '')

        system         = yaml.load(open(SYSTEM_PATH))
        self.resources = system.get('resources', [])
        self.template  = system.get('template', '')

    def parse_request(self, request):
        self.parsed_elements = {}

        # Parse and store elements and resources
        for element in self.elements + self.resources:
            name  = element['name']
            etype = element['type']
            value = element.get('value', None)

            if etype == 'string':
                self.parsed_elements[name] = request.get_argument(name, value)
            elif etype == 'list':
                self.parsed_elements[name] = ' '.join(request.get_arguments(name, []))
            elif etype == 'number':
                self.parsed_elements[name] = request.get_argument(name, value)
            elif etype == 'file':
                self.parsed_elements[name] = request.get_argument(name, value)

        # Parse and store any file elements
        for name, uploads in request.request.files.items():
            self.parsed_elements[name] = uploads[0]['filename']

        # Expand any elements that depend on other elements
        for key, value in self.parsed_elements.items():
            if hasattr(value, 'format'):
                self.parsed_elements[key] = value.format(**self.parsed_elements)

        # Generate TranscribedCommand
        self.parsed_elements['TranscribedCommand'] = self.formula.format(**self.parsed_elements)

    def transcribe(self, stream):
        stream.write(self.template.format(**self.parsed_elements))


def load_wizards(path=None):
    path    = path or WIZARDS_PATH
    wizards = {}

    for name in os.listdir(path):
        if not name.endswith('.yaml'):
            continue

        base   = os.path.splitext(name)[0]
        wizard = Wizard(os.path.join(path, name))
        if wizard.elements:
            wizards[base] = wizard

    return wizards

# vim: sts=4 sw=4 ts=8 expandtab ft=python
