''' Obelisk - Database '''

import datetime
import getpass
import os
import sqlite3
import time

from config import DATABASE_PATH

# SQL

SQL_CREATE_SCROLLS_TABLE = '''
CREATE TABLE IF NOT EXISTS Scrolls (
    id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name        TEXT NOT NULL UNIQUE,
    path        TEXT NOT NULL UNIQUE,
    ctime       INTEGER NOT NULL,
    mtime       INTEGER NOT NULL
)
'''
SQL_GET_SCROLL         = '''
SELECT Scrolls.id, name, Scrolls.path, ctime, mtime, Count(Minions.id) AS summons
FROM Scrolls
LEFT OUTER JOIN Minions ON Minions.scroll = Scrolls.id
WHERE Scrolls.id=?
'''
SQL_GET_SCROLLS        = '''
SELECT Scrolls.id, name, Scrolls.path, ctime, mtime, Count(Minions.id) AS summons
FROM Scrolls
LEFT OUTER JOIN Minions ON Minions.scroll = Scrolls.id
GROUP BY Scrolls.id
'''
SQL_ADD_SCROLL         = 'INSERT INTO Scrolls (name, path, ctime, mtime) VALUES (?, ?, ?, ?)'
SQL_ADD_SCROLL_RESULT  = 'SELECT id FROM Scrolls WHERE rowid=(SELECT last_insert_rowid())'
SQL_UPDATE_SCROLL_PATH = 'UPDATE Scrolls SET path=?, mtime=? WHERE id=?'

SQL_CREATE_MINIONS_TABLE = '''
CREATE TABLE IF NOT EXISTS Minions (
    id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    path        TEXT NOT NULL UNIQUE,
    job_id      TEXT,
    start_time  INTEGER NOT NULL,
    stop_time   INTEGER NOT NULL,
    status      TEXT NOT NULL,
    scroll      INTEGER NOT NULL,
    FOREIGN KEY(scroll) REFERENCES Scrolls(id)
)
'''
SQL_GET_MINION         = '''
SELECT Minions.id, Minions.path, job_id, start_time, stop_time, status, scroll as scroll_id, Scrolls.name as scroll_name
FROM Minions
JOIN Scrolls ON Minions.scroll = Scrolls.id
WHERE Minions.id=?
'''
SQL_GET_MINIONS        = '''
SELECT Minions.id, Minions.path, job_id, start_time, stop_time, status, scroll as scroll_id, Scrolls.name as scroll_name
FROM Minions
JOIN Scrolls ON Minions.scroll = Scrolls.id
'''
SQL_ADD_MINION              = 'INSERT INTO Minions (path, start_time, stop_time, status, scroll) VALUES (?, ?, ?, ?, ?)'
SQL_ADD_MINION_RESULT       = 'SELECT id FROM Minions WHERE rowid=(SELECT last_insert_rowid())'
SQL_UPDATE_MINION_JOB_ID    = 'UPDATE Minions SET job_id=? WHERE id=?'
SQL_UPDATE_MINION_PATH      = 'UPDATE Minions SET path=? WHERE id=?'
SQL_UPDATE_MINION_STATUS    = 'UPDATE Minions SET status=? WHERE job_id=?'
SQL_UPDATE_MINION_STOP_TIME = 'UPDATE Minions SET stop_time=? WHERE job_id=?'

# Database

class Database(object):
    def __init__(self, path=None):
        self.path = path or DATABASE_PATH
        self.conn = sqlite3.connect(self.path)
        self.conn.row_factory = sqlite3.Row

        with self.conn:
            curs = self.conn.cursor()
            curs.execute(SQL_CREATE_SCROLLS_TABLE)
            curs.execute(SQL_CREATE_MINIONS_TABLE)

    def scroll(self, scroll_id):
        with self.conn:
            curs = self.conn.cursor()
            curs.execute(SQL_GET_SCROLL, (scroll_id,))
            return curs.fetchone()

    def scrolls(self):
        with self.conn:
            curs = self.conn.cursor()
            return curs.execute(SQL_GET_SCROLLS)

    def add_scroll(self, name, path):
        with self.conn:
            current_time = int(time.time())
            curs = self.conn.cursor()
            curs.execute(SQL_ADD_SCROLL, (name, path, current_time, current_time))
            curs.execute(SQL_ADD_SCROLL_RESULT)
            return curs.fetchone()[0]

    def update_scroll_path(self, scroll_id, path):
        with self.conn:
            curs = self.conn.cursor()
            curs.execute(SQL_UPDATE_SCROLL_PATH, (path, int(time.time()), scroll_id))

    def minion(self, minion_id):
        with self.conn:
            curs = self.conn.cursor()
            curs.execute(SQL_GET_MINION, (minion_id,))
            return curs.fetchone()

    def minions(self):
        with self.conn:
            curs = self.conn.cursor()
            return curs.execute(SQL_GET_MINIONS)

    def add_minion(self, scroll, path):
        with self.conn:
            current_time = int(time.time())
            curs = self.conn.cursor()
            curs.execute(SQL_ADD_MINION, (path, current_time, 0, 'Q', scroll))
            curs.execute(SQL_ADD_MINION_RESULT)
            return curs.fetchone()[0]

    def update_minion_path(self, minion_id, path):
        with self.conn:
            curs = self.conn.cursor()
            curs.execute(SQL_UPDATE_MINION_PATH, (path, minion_id))

    def update_minion_job_id(self, minion_id, job_id):
        with self.conn:
            curs = self.conn.cursor()
            curs.execute(SQL_UPDATE_MINION_JOB_ID, (job_id, minion_id))

    def update_minion_status(self, job_id, status):
        with self.conn:
            curs = self.conn.cursor()
            curs.execute(SQL_UPDATE_MINION_STATUS, (status, job_id))

    def update_minion_stop_time(self, job_id, stop_time):
        with self.conn:
            curs = self.conn.cursor()
            curs.execute(SQL_UPDATE_MINION_STOP_TIME, (stop_time, job_id))

# Database

def update_database_from_qstat(database):
    jobs   = {}
    job_id = None

    for line in os.popen('qstat -f -u {}'.format(getpass.getuser()), 'r'):
        line = line.strip()
        if line.startswith('Job Id'):
            job_id = line.split(':')[-1].strip()
            jobs[job_id] = {}
        elif line.startswith('job_state'):
            jobs[job_id]['status']     = line.split('=', 1)[-1].strip()
        elif line.startswith('comp_time'):
            jobs[job_id]['stop_time'] = time.mktime(datetime.datetime.strptime(line.split('=', 1)[-1].strip(), '%a %b %d %H:%M:%S %Y').timetuple())

    for job_id, job_data in jobs.items():
        if 'status' in job_data:
            database.update_minion_status(job_id, job_data['status'])
        if 'stop_time' in job_data:
            database.update_minion_stop_time(job_id, job_data['stop_time'])

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
