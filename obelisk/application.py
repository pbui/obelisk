''' Obelisk - Application '''

import logging
import os
import socket

import tornado.ioloop
import tornado.web

from config   import DATABASE_PATH, MINIONS_PATH, SCROLLS_PATH, TEMPLATES_PATH, WIZARDS_PATH
from database import Database, update_database_from_qstat
from handler  import DashboardHandler, MinionHandler, MinionFileHandler, ScrollHandler, WizardHandler
from wizard   import load_wizards

# Obelisk Application

class ObeliskApplication(tornado.web.Application):
    HTTP_PORT = 52051

    def __init__(self, port=None, **settings):
        settings.update({
            'debug'         : True,
            'template_path' : TEMPLATES_PATH,
        })
        tornado.web.Application.__init__(self, **settings)

        self.port          = port or self.HTTP_PORT
        self.ioloop        = tornado.ioloop.IOLoop.instance()
        self.logger        = logging.getLogger()
        self.database      = Database(settings.get('database_path', DATABASE_PATH))
        self.scrolls_path  = settings.get('scrolls_path', SCROLLS_PATH)
        self.minions_path  = settings.get('minions_path', MINIONS_PATH)
        self.wizards_path  = settings.get('wizards_path', WIZARDS_PATH)

        if not os.path.exists(self.minions_path):
            os.makedirs(self.minions_path)

        if not os.path.exists(self.scrolls_path):
            os.makedirs(self.scrolls_path)

        self.wizards = load_wizards(self.wizards_path)

        self.add_handlers('', [
            (r'/'                       , DashboardHandler),
            (r'/minion/([^/]*)'         , MinionHandler),
            (r'/minion/([^/]*)/([^/]*)' , MinionFileHandler),
            (r'/scroll/([^/]*)'         , ScrollHandler),
            (r'/wizard/([^/]*)'         , WizardHandler),
        ])

        self.database_updater = tornado.ioloop.PeriodicCallback(
                lambda: update_database_from_qstat(self.database),
                5000,
        )

    @property
    def ip(self):
        ip = '127.0.0.1'
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('8.8.8.8', 53))  # Google DNS
            ip = s.getsockname()[0]
            s.close()
        except socket.error:
            self.logger.warn('Could not find route to internet')

        return ip

    @property
    def url(self):
        return 'http://{}:{}'.format(self.ip, self.port)

    def run(self):
        self.listen(self.port, xheaders=True)
        self.logger.info('Listening on {}'.format(self.url))

        self.database_updater.start()

        self.ioloop.start()

# vim: sts=4 sw=4 ts=8 expandtab ft=python
