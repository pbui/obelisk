''' Obelisk - Handlers '''

import os
import shutil
import subprocess
import tempfile
import traceback

import tornado.web

from config import SCROLLS_PATH, MINIONS_PATH
from util   import determine_file_mimetype, timediff_as_string
from distutils.dir_util import copy_tree

# BaseHandler

class BaseHandler(tornado.web.RequestHandler):
    def write_error(self, status_code, **kwargs):
        params = {
            'status_code': status_code,
            'traceback'  : traceback.format_exc(kwargs.get('exc_info'))
        }
        self.render('error.tmpl', **params)


# Dashboard Handler

class DashboardHandler(BaseHandler):
    def get(self):
        params = {
            'wizards'  : self.application.wizards,
            'scrolls'  : self.application.database.scrolls(),
            'minions'  : self.application.database.minions(),
            'timediff' : timediff_as_string,
        }
        self.render('dashboard.tmpl', **params)


# Minion Handler

class MinionHandler(BaseHandler):
    def get(self, minion_id=None):
        if minion_id:
            minion_path = os.path.join(MINIONS_PATH, str(minion_id))
            params = {
                'files'   : sorted(os.listdir(minion_path)),
                'minion'  : self.application.database.minion(minion_id),
                'timediff': timediff_as_string,
            }
            self.render('minion.tmpl', **params)
        else:
            params = {
                'minions' : self.application.database.minions(),
                'timediff': timediff_as_string,
            }
            self.render('minions.tmpl', **params)


# MinionFileHandler

class MinionFileHandler(BaseHandler):
    def get(self, minion_id=None, file_name=None):
        minion_id = minion_id or ''
        file_name = file_name or ''
        file_path = os.path.join(MINIONS_PATH, minion_id, file_name)
        file_mime = determine_file_mimetype(file_path)

        if not file_name or not minion_id or not os.path.exists(file_path):
            raise tornado.web.HTTPError(404)

        try:
            with open(file_path, "rb") as file_stream:
                self.set_header('Content-Type', file_mime)
                #self.set_header('Content-Disposition', 'attachment; filename={}'.format(file_name))

                self.write(file_stream.read())
        except IOError:
            raise tornado.web.HTTPError(500)


# Scroll Handler

class ScrollHandler(BaseHandler):
    def get(self, scroll_id=None):
        if scroll_id:
            self.render('scroll.tmpl', scroll=self.application.database.scroll(scroll_id))
        else:
            params = {
                'scrolls' : self.application.database.scrolls(),
                'timediff': timediff_as_string,
            }
            self.render('scrolls.tmpl', **params)

    def post(self, scroll_id=None):
        # Add scroll to db
        minion_id = self.application.database.add_minion(scroll_id, scroll_id)

        # Change path
        minion_path = os.path.join(MINIONS_PATH, str(minion_id))
        self.application.database.update_minion_path(minion_id, minion_path)

        # Create minion's space
        os.makedirs(minion_path)

        # Copy scroll
        scroll_dir = os.path.join(SCROLLS_PATH, str(scroll_id))
        copy_tree(scroll_dir, minion_path)

        # Run qsub in minion
        job_id = subprocess.check_output('qsub scroll.sh', shell=True, cwd=minion_path).strip()

        # Update minion job_id
        self.application.database.update_minion_job_id(minion_id, job_id)

        # Redirect to minion page
        self.redirect('/minion/{}'.format(minion_id))


# Wizard Handler

class WizardHandler(BaseHandler):
    def get(self, wizard_id=None):
        if wizard_id:
            self.render('wizard.tmpl', wizard=self.application.wizards[wizard_id])
        else:
            self.render('wizards.tmpl', wizards=self.application.wizards)

    def post(self, wizard_id=None):
        wizard = self.application.wizards[wizard_id]
        wizard.parse_request(self)

        # Add scroll to database with temporary path
        scroll_id = self.application.database.add_scroll(wizard.parsed_elements.get('Name', 'Unnamed'), self.get_argument('Name', []))

        # Make scroll folder
        scroll_dir = os.path.join(SCROLLS_PATH, str(scroll_id))
        os.makedirs(scroll_dir)

        # Transcribe scroll to script and update database
        # TODO: Check for failures!
        scroll_path = os.path.join(scroll_dir, 'scroll.sh')
        with open(scroll_path, 'w') as scroll_file:
            wizard.transcribe(scroll_file)

        self.application.database.update_scroll_path(scroll_id, scroll_path)

        # Store input files in scroll directory
        # TODO: Check for failures and Sanitize!
        for name, uploads in self.request.files.items():
            for upload in uploads:
                upload_name = upload['filename']
                upload_path = os.path.join(scroll_dir, upload_name)

                with open(upload_path, 'w') as upload_file:
                    upload_file.write(upload['body'])

        # Redirect to scroll page
        self.redirect('/scroll/{}'.format(scroll_id))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
