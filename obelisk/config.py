''' Obelisk - Configuration '''

import os

INVENTORY_PATH = os.environ.get('OBELISK_INVENTORY', os.path.join(os.curdir, 'inventory'))
DATABASE_PATH  = os.path.join(INVENTORY_PATH, 'database')
MINIONS_PATH   = os.path.join(INVENTORY_PATH, 'minions')
SCROLLS_PATH   = os.path.join(INVENTORY_PATH, 'scrolls')
TEMPLATES_PATH = os.path.join(os.path.dirname(__file__), 'templates')
WIZARDS_PATH   = os.path.join(INVENTORY_PATH, 'wizards')

# vim: sts=4 sw=4 ts=8 expandtab ft=python
