''' Obelisk - Utilities '''

import mimetypes
import os
import pwd
import time

# Byte utilites

BYTES_TABLE = [
    (1 << 50, 'PB'),
    (1 << 40, 'TB'),
    (1 << 30, 'GB'),
    (1 << 20, 'MB'),
    (1 << 10, 'KB'),
]

def humanize_bytes(bytes):
    for value, suffix in BYTES_TABLE:
        if bytes >= value:
            return '{:.2f} {}'.format(float(bytes) / value, suffix)

    return '{} bytes'.format(bytes)

# Mimetype utilities

def determine_file_mimetype(path):
    mimetype, _ = mimetypes.guess_type(path)
    if mimetype is None:
        mimetype = 'text/plain'
    return mimetype

# Time utilities

def timespan_as_string(time_span):
    time_unit  = 'second'
    time_pairs = [
        (60, 'minute'),
        (60, 'hour'),
        (24, 'day'),
    ]

    for limit, unit in time_pairs:
        if time_span < limit:
            break

        time_span = time_span / limit
        time_unit = unit

    time_span = int(round(time_span))
    if time_span != 1:
        time_unit = time_unit + 's'

    return '{0} {1}'.format(time_span, time_unit)

def timestamp_as_string(timestamp):
    if isinstance(timestamp, str):
        return timestamp

    return time.ctime(int(timestamp))

def timediff_as_string(start, end=None):
    start     = start or time.time()
    end       = end or time.time()
    time_diff = end - start
    return timespan_as_string(time_diff)

# User utilities

def current_user():
    return os.environ.get('USER', pwd.getpwuid(os.getuid()).pw_name)

# vim: sts=4 sw=4 ts=8 expandtab ft=python
